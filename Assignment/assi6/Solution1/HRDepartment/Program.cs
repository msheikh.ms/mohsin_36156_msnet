﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;
using ClassLibrary;

namespace assignment6
{
    class Program
    {
        static void Main(string[] args)
        {

            FileStream fs = new FileStream(@"D:\MSNET\assignments\Assignment_6\Solution1\dept.csv", FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(fs);
            Dictionary<int, Department> deptList = new Dictionary<int, Department>();
            string deptString;
            while ((deptString = reader.ReadLine()) != null)
            {
                string[] department = deptString.Split(',');
                Department dept = new Department();
                dept.Deptno = Convert.ToInt32(department[0]);
                dept.DeptName = department[1];
                dept.Location = department[2];
                deptList.Add(dept.Deptno, dept);
            }
            reader.Close();
            fs.Close();

            FileStream fs1 = new FileStream(@"D:\MSNET\assignments\Assignment_6\Solution1\emp.csv", FileMode.Open, FileAccess.Read);
            StreamReader reader1 = new StreamReader(fs1);
            Dictionary<int, Employee> empList = new Dictionary<int, Employee>();
            string empString;
            while ((empString = reader1.ReadLine()) != null)
            {

                Employee emp = new Employee();
                string[] empDetails = empString.Split(',');
                emp.Id = Convert.ToInt32(empDetails[0]);
                emp.name = empDetails[1];
                emp.designation = empDetails[2];
                emp.salary = float.Parse(empDetails[3]);
                emp.commission = float.Parse(empDetails[4]);
                emp.deptNo = Convert.ToInt32(empDetails[5]);
                empList.Add(emp.Id, emp);

            }
            reader1.Close();
            fs1.Close();
            char c;
            do
            {
                Console.WriteLine("enter the choice for " +
                                  "1:location with single department\n" +
                                  "2:department with no emp\n" +
                                  "3:total salary\n" + 
                                  "4:get All Employeee of specific department\n" +
                                  "5:department wise staff count\n" +
                                  "6:department wise avergae salary\n" + 
                                  "7:department wise minimum salary\n" +
                                  "8:employee name along with department name");
                int choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        List<string> location = LocationWithSingleDept(deptList);
                        Console.WriteLine("\nLocation with single department::");
                        foreach (string loc in location)
                        {
                            Console.WriteLine("\n" + loc);
                        }
                        break;
                    case 2:
                        List<string> dName = FindDeptsWithNoEmps(deptList, empList);
                        Console.WriteLine("\ndepartment with no employee::");
                        foreach (string departname in dName)
                        {
                            Console.WriteLine("\n" + departname);
                        }
                        break;
                    case 3:
                        Console.WriteLine("\ntotal salary of employees={0}", TotalSalEmp(empList));
                        break;
                    case 4:
                        Console.WriteLine("enter the department no");
                        int dno = Convert.ToInt32(Console.ReadLine());
                        List<Employee> emp1 = GetAllEmployeesByDept(empList, dno);
                        foreach (Employee e in emp1)
                        {
                            Console.WriteLine(e.details());
                        }
                        break;
                    case 5:
                        Dictionary<int, Double> empCount = DeptwiseStaffCount(deptList, empList);
                        Console.WriteLine("\ndepartment wise staff count::");
                        foreach (int keys in empCount.Keys)
                        {
                            Console.WriteLine("\ndepartment id={0}   staff count={1}", keys, empCount[keys]);
                        }
                        break;
                    case 6:
                        Dictionary<int, Double> deptAvgSal = DeptwiseAvgSal(deptList, empList);
                        Console.WriteLine("\ndepartment wise average salary::");
                        foreach (int keys in deptAvgSal.Keys)
                        {
                            Console.WriteLine("\ndepartment id={0}   aveerage salary={1}", keys, deptAvgSal[keys]);
                        }
                        break;
                    case 7:
                        Dictionary<int, Double> deptMinSal = DeptwiseMinSal(deptList, empList);
                        Console.WriteLine("\nminimum sal in the department::");
                        foreach (int keys in deptMinSal.Keys)
                        {
                            Console.WriteLine("\ndepartment id={0}   minimum salary={1}", keys, deptMinSal[keys]);
                        }
                        break;

                    case 8:
                        Dictionary<int, KeyValuePair<string, string>> getempinfo = GetEmpInfo(deptList, empList);
                        Console.WriteLine("\nemployees name along with department names::");

                        foreach (int keys in getempinfo.Keys)
                        {
                            Console.WriteLine("\nemployee no.={0}   department name={1}  employee name={2}", keys, getempinfo[keys].Key, getempinfo[keys].Value);

                        }

                        break;
                    default:
                        Console.WriteLine("\nInvalid choice");
                        break;
                }
                Console.WriteLine("\ndo u want to continue if yes then press y else press n::");
                c = Convert.ToChar(Console.ReadLine());
            } while (c == 'y');



            Console.ReadLine();
        }

        public static List<string> LocationWithSingleDept(Dictionary<int, Department> deptList)
        {
            var loc = (from department in deptList.Values
                       select department.Location).ToList();


            //List<string> loc = new List<string>();
            //foreach (int key in deptList.Keys)
            //{
            //    loc.Add(deptList[key].Location);
            //}
            List<string> list1 = loc.Distinct().ToList();
            List<string> location = new List<string>();
            for (int i = 0; i < list1.Count; i++)
            {
                int k = 0;
                for (int j = 0; j < loc.Count; j++)
                {
                    if (list1[i] == loc[j])
                    {
                        k++;
                    }
                    if (k >= 2)
                    {
                        break;
                    }
                }
                if (k == 1)
                {
                    location.Add(list1[i]);
                }
            }
            return location;
        }

        public static List<string> FindDeptsWithNoEmps(Dictionary<int, Department> deptList, Dictionary<int, Employee> empList)
        {
            var deptId = (from employee in empList.Values
                          select employee.deptNo).ToList();

            //List<int> deptId = new List<int>();
            //foreach (int key in empList.Keys)
            //{
            //    deptId.Add(empList[key].deptNo);
            //}
            deptId = deptId.Distinct().ToList();


            var deptNo = (from department in deptList.Values
                          select department.Deptno).ToList();

            //List<int> deptNo = new List<int>();
            //foreach (int key in deptList.Keys)
            //{
            //    deptNo.Add(deptList[key].Deptno);
            //}
            List<int> deptNoEmp = new List<int>();
            for (int i = 0; i < deptNo.Count; i++)
            {
                int k = 0;
                for (int j = 0; j < deptId.Count; j++)
                {
                    if (deptNo[i] == deptId[j])
                    {
                        k = 1;
                        break;
                    }
                }
                if (k == 0)
                {
                    deptNoEmp.Add(deptNo[i]);
                }
            }
            List<string> deptNEmp = new List<string>();
            foreach (int id in deptNoEmp)
            {
                deptNEmp.Add(deptList[id].DeptName);
            }
            return deptNEmp;
        }

        public static float TotalSalEmp(Dictionary<int, Employee> empList)
        {
            var sal = new List<float>();
            sal = (from employee in empList.Values
                   select employee.salary).ToList();
            float totalSal = 0;

            for (int i = 0; i < sal.Count; i++)
            {
                totalSal += sal[i];
            }

            var commi = new List<float>();
            commi = (from employee in empList.Values
                     select employee.commission).ToList();


            for (int i = 0; i < commi.Count; i++)
            {
                totalSal += commi[i];
            }

            return totalSal;
            //float totalSal = 0;
            //foreach (int keys in empList.Keys)
            //{
            //    totalSal += empList[keys].salary;
            //    totalSal += empList[keys].commission;
            //}
            //return totalSal;
        }

        public static List<Employee> GetAllEmployeesByDept(Dictionary<int, Employee> empList, int dept)
        {
            var emp = new List<Employee>();
            emp = (from employee in empList.Values
                   where (employee.deptNo == dept)
                   select employee).ToList();
            return emp;

            //List<Employee> emp = new List<Employee>();
            //foreach (int keys in empList.Keys)
            //{
            //    if (empList[keys].deptNo == dept)
            //    {
            //        emp.Add(empList[keys]);
            //    }
            //}
            //return emp;
        }

        public static Dictionary<int, Double> DeptwiseStaffCount(Dictionary<int, Department> deptList, Dictionary<int, Employee> empList)
        {
            Dictionary<int, Double> result = new Dictionary<int, Double>();

            var deptNo = new List<int>();
            deptNo = (from department in deptList.Values
                      select department.Deptno).ToList();

            //List<int> deptNo = new List<int>();
            //foreach (int key in deptList.Keys)
            //{
            //    deptNo.Add(deptList[key].Deptno);
            //}

            var deptId = new List<int>();
            deptId = (from employee in empList.Values
                      select employee.deptNo).ToList();

            //List<int> deptId = new List<int>();
            //foreach (int key in empList.Keys)
            //{
            //    deptId.Add(empList[key].deptNo);
            //}

            for (int i = 0; i < deptNo.Count; i++)
            {
                double k = 0;
                for (int j = 0; j < deptId.Count; j++)
                {
                    if (deptNo[i] == deptId[j])
                    {
                        k++;
                    }
                }
                result.Add(deptNo[i], k);
            }
            return result;
        }

        public static Dictionary<int, Double> DeptwiseAvgSal(Dictionary<int, Department> deptList, Dictionary<int, Employee> empList)
        {
            Dictionary<int, Double> result = new Dictionary<int, Double>();

            var deptNo = new List<int>();
            deptNo = (from department in deptList.Values
                      select department.Deptno).ToList();

            //List<int> deptNo = new List<int>();
            //foreach (int key in deptList.Keys)
            //{
            //    deptNo.Add(deptList[key].Deptno);
            //}

            var emp = new List<Employee>();
            emp = (from employee in empList.Values
                   select employee).ToList();

            //List<Employee> emp = new List<Employee>();
            //foreach (int keys in empList.Keys)
            //{
            //    emp.Add(empList[keys]);
            //}
            Dictionary<int, Double> empCount = DeptwiseStaffCount(deptList, empList);
            for (int i = 0; i < deptNo.Count; i++)
            {
                float sal = 0;
                for (int j = 0; j < emp.Count; j++)
                {
                    if (deptNo[i] == emp[j].deptNo)
                    {
                        sal += emp[j].salary;
                    }
                }
                float avgSal = sal / float.Parse(empCount[deptNo[i]].ToString());
                result.Add(deptNo[i], avgSal);
            }
            return result;
        }

        public static Dictionary<int, Double> DeptwiseMinSal(Dictionary<int, Department> deptList, Dictionary<int, Employee> empList)
        {
            Dictionary<int, Double> result = new Dictionary<int, Double>();


            var deptNo = new List<int>();
            deptNo = (from department in deptList.Values
                      select department.Deptno).ToList();

            //List<int> deptNo = new List<int>();
            //foreach (int key in deptList.Keys)
            //{
            //    deptNo.Add(deptList[key].Deptno);
            //}


            var emp = new List<Employee>();
            emp = (from employee in empList.Values
                   select employee).ToList();

            //List<Employee> emp = new List<Employee>();
            //foreach (int keys in empList.Keys)
            //{
            //    emp.Add(empList[keys]);
            //}

            for (int i = 0; i < deptNo.Count; i++)
            {
                float sal = float.MaxValue;
                for (int j = 0; j < emp.Count; j++)
                {
                    if (deptNo[i] == emp[j].deptNo)
                    {
                        if (sal > emp[j].salary)
                        {
                            sal = emp[j].salary;
                        }
                    }
                }
                if (sal == float.MaxValue)
                {
                    sal = 0;
                }

                result.Add(deptNo[i], sal);

            }
            return result;
        }

        public static Dictionary<int, KeyValuePair<string, string>> GetEmpInfo(Dictionary<int, Department> DeptList, Dictionary<int, Employee> EmpList)
        {
            Dictionary<int, KeyValuePair<string, string>> result = new Dictionary<int, KeyValuePair<string, string>>();


            var dept = new List<Department>();
            dept = (from department in DeptList.Values
                    select department).ToList();
            dept = dept.Distinct().ToList();
            //List<int> deptNo = new List<int>();
            //foreach (int key in DeptList.Keys)
            //{
            //    deptNo.Add(DeptList[key].deptno);
            //}


            var emp = new List<Employee>();
            emp = (from employee in EmpList.Values
                   select employee).ToList();

            //List<employee> emp = new List<employee>();
            //foreach (int keys in EmpList.Keys)
            //{
            //    emp.Add(EmpList[keys]);
            //}

            for (int i = 0; i < dept.Count; i++)
            {

                for (int j = 0; j < emp.Count; j++)
                {
                    if (dept[i].Deptno == emp[j].deptNo)
                    {
                        result.Add(emp[j].Id, new KeyValuePair<string, string>(dept[i].DeptName, emp[j].name));
                    }
                }


            }
            return result;
        }
    }
}
