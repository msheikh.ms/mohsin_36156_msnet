﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{

    #region  employee class
    public class Employee 
    {
        private int id;
        private string Name;
        private string Designation;
        private float Salary;
        private float Commission;
        private int DeptNo;

        public Employee()
        {
            id = 0;
            Name = "";
            Designation = "";
            Salary = 0;
            Commission = 0;
            DeptNo = 0;
        }

        public Employee(int id, string name, string designation, int sal, int comm, int dept)
        {
            this.id = id;
            Name = name;
            Designation = designation;
            Salary = sal;
            Commission = comm;
            DeptNo = dept;
        }
        public int deptNo
        {
            get { return DeptNo; }
            set { DeptNo = value; }
        }

        public float commission
        {
            get { return Commission; }
            set { Commission = value; }
        }

        public float salary
        {
            get { return Salary; }
            set { Salary = value; }
        }


        public string designation
        {
            get { return Designation; }
            set { Designation = value; }
        }


        public string name
        {
            get { return Name; }
            set { Name = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string details()
        {
            return ("\nid: " + Id.ToString() + "\nname: " + name + "\ndesignation: " + designation + "\nsalary: " + salary.ToString() + "\ncommission: " + commission.ToString() + "\ndepartment no: " + deptNo.ToString());
        }
    }
    #endregion

    #region department class
    public class Department
    {
        private int _Deptno;

        private string _DeptName;

        private string _Location;

        public Department()
        {
            _DeptName = "";
            _Deptno = 0;
            _Location = "";
        }

        public Department(int deptno, string deptname, string location)
        {
            _DeptName = deptname;
            _Deptno = deptno;
            _Location = location;
        }
        public string Location
        {
            get { return _Location; }
            set
            {
                _Location = value;
            }
        }

        public string DeptName
        {
            get { return _DeptName; }
            set
            {
                _DeptName = value;
            }
        }

        public int Deptno
        {
            get { return _Deptno; }
            set
            {
                _Deptno = value;
            }
        }

        public void accept()
        {
            Console.WriteLine("enter the dept no");
            Deptno = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("enter the dept name");
            DeptName = Console.ReadLine();
            Console.WriteLine("enter the dept location");
            Location = Console.ReadLine();
        }

        public void display()
        {
            Console.WriteLine("\nDept no={0}", Deptno);
            Console.WriteLine("\nDept name={0}", DeptName);
            Console.WriteLine("\nDept location={0}", Location);
        }

        public string details()
        {
            return ("\ndept no: " + Deptno.ToString() + "\ndept name: " + DeptName + "\ndept location: " + Location);
        }
    }
    #endregion
}
