﻿using System;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

struct books
{
    public string title;
    public bool outofstock;
    public string author;
    
    public char index;
    public double price;

    public void getValues(string btitle, bool boutofstock, string bauthor, char bindex, double bprice)
    {
        title = btitle;
        outofstock = boutofstock;
        author = bauthor;
        index = bindex;
        price = bprice;
    }

    public void acceptData()
    {
        Console.WriteLine("enter titlt of book");
        this.title = Console.ReadLine();
        Console.WriteLine("enter avablity of book in stock(true/false)");
        this.outofstock = Convert.ToBoolean(Console.ReadLine());
        Console.WriteLine("enter author of book");
        this.author= Console.ReadLine();
        Console.WriteLine("enter index of book");
        this.index = Convert.ToChar(Console.ReadLine());
        Console.WriteLine("enter Price of book");
        this.price = Convert.ToDouble(Console.ReadLine());
    }

    public void display()
    {
        Console.WriteLine("title:"+this.title);
        Console.WriteLine("in stock:"+this.outofstock);
        Console.WriteLine("Author:"+this.author);
        Console.WriteLine("Index:"+this.index);
        Console.WriteLine("Price:"+this.price);
    }
};

namespace assi2
{
    class Program
    {
        static void Main(string[] args)
        {
            /* books b11;
             b1.title = "c#";
             b1.outofstock = false;
             b1.author = "xyz";
             b1.index = 'c';
             b1.price = 2500;*/

            books b1 = new books();
            b1.acceptData();
            b1.getValues("c#", false, "XYZ",'c',2500);
            b1.display();
            Console.ReadLine();

        }




    }
}
