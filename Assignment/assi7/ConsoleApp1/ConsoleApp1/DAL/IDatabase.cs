﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp1.POCO;

namespace ConsoleApp1.DAL
{
    public interface IDatabase
    {
        List<Emp> Select();

        int Insert(Emp emp);
        int Update(Emp emp);
        int Delete(Emp emp);
    }
}
