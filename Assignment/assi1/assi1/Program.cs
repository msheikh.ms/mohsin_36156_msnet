﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ClassLibrary1;

namespace assi1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("-:calci:-");
            string iscontinue = "y";
            do
            {
                Console.WriteLine("enter your choise");
                Console.WriteLine("1.add;2.sub;3.mul;4.div");
                int choise = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter value of x");
                int x = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter value of y");
                int y = Convert.ToInt32(Console.ReadLine());

                int result = 0 ;

                Maths obj = new Maths();

                switch(choise)
                {
                    case 1:
                        //  result = x + y;
                        result = obj.Add(x, y);
                        break;
                    case 2:
                        // result = x - y;
                        result = obj.sub(x, y);
                        break;
                    case 3:
                        // result = x * y;
                        result = obj.mul(x, y);
                        break;
                    case 4:
                        //   result = x / y;
                        result = obj.div(x, y);
                        break;

                    default :
                        Console.WriteLine("invalide choise"); 
                        break;
                }

                Console.WriteLine("result=" + result);

                Console.WriteLine("Would you like to continue? y/n");
                iscontinue = Console.ReadLine();


            } while (iscontinue=="y");
            

            Console.ReadLine();
        }
    }
}
