﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Date
{
    class Program
    {
        static void Main(string[] args)
        {
            Date date = new Date();
            

            Console.WriteLine("enter day");
            int day = Convert.ToInt32(Console.ReadLine());

        
            Console.WriteLine("enter month");
            int month = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("enter year");
            int year = Convert.ToInt32(Console.ReadLine());


            Date date1 = new Date(day, month, year);
            
            object obj= date1.GetDetails();

            Console.WriteLine(Convert.ToString(obj));

            Console.ReadLine();
        }
    }
   public class Date
    {
        private int _day;

        public int day
        {
            get { return _day; }
            set 
            {
                if (value >= 1 && value <= 31)
                {

                _day = value;
                }
                else
                {
                    Console.WriteLine("day should be between 1-31");
                }
            }
        }

        private int _month;

        public int month
        {
            get { return _month; }
            set 
            {
                if (value >= 1 && value <= 12)
                {

                _month = value;
                  
                }
                else
                {
                    Console.WriteLine("month should be between 1-12");
                }
            }
        }

        private int _year;

        public int year
        {
            get { return _year; }
            set
            {
                if(value==20 && value == 21)
                {

                _year = value;
                }
                else
                {
                    Console.WriteLine("year should be between 20 and 21");
                }
            }
        }

        public  Date()
        {
            this._day=0;
            this._month=0;
            this._year = 0;
        }
      public  Date(int day,int month,int year)
        {

            this.day = day;
            this.month = month;
            this.year = year;
        }

        public string GetDetails()
        {
            return this.day + "/" + this.month + "/" + this.year;
        }

        /*public static string GetDifference(int year_1 ,int year_2)
        {
            return Convert.ToString(year_1 - year_2);
        }
*/

    }
}
