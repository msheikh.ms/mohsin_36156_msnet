﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMSLib;
namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            
            string ToContinue = "y";
            do
            {
                Console.WriteLine("Enter Employee Types");
                Console.WriteLine("1: Employee , 2: SalesPerson Employee, 3: WageEmployees,4: Department ");

                int choice = Convert.ToInt32(Console.ReadLine());


                switch (choice)
                {
                    case 1:
                        Employee emp = new Employee();
                        emp.AccepDetails();
                        Console.WriteLine(emp.ToString());
                        break;
                    case 2:
                        SalesPerson Sp = new SalesPerson();
                        Sp.AccepDetails();
                        Console.WriteLine(Sp.ToString());
                        break;
                    case 3:
                        WageEmp W = new WageEmp();
                        W.AccepDetails();
                        Console.WriteLine(W.ToString());
                        break;
                    case 4:
                        Department d = new Department();
                        d.Acceptdetails();
                        d.ToString();
                        break;
                    default:
                        break;
                }
                Console.WriteLine("would u like to continue? enter y/n ");
                ToContinue = Console.ReadLine();
            } while (ToContinue == "y");

            Console.ReadLine();

        }
    }
}
