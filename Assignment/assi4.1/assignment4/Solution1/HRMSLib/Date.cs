﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class Date
    {
        private int _day;

        public int day
        {
            get { return _day; }
            set
            {
                if (value >= 1 && value <= 31)
                {

                    _day = value;
                }
                else
                {
                    Console.WriteLine("day should be between 1-31");
                }
            }
        }

        private int _month;

        public int month
        {
            get { return _month; }
            set
            {
                if (value >= 1 && value <= 12)
                {

                    _month = value;

                }
                else
                {
                    Console.WriteLine("month should be between 1-12");
                }
            }
        }

        private int _year;

        public int year
        {
            get { return _year; }
            set
            {
                if (value == 2020 || value == 2021)
                {

                    _year = value;
                }
                else
                {
                    Console.WriteLine("year should be between 200 and 2021");
                }
            }
        }

        public Date()
        {
            this._day = 0;
            this._month = 0;
            this._year = 0;
        }
        public Date(int day, int month, int year)
        {

            this.day = day;
            this.month = month;
            this.year = year;
        }

        public override string  ToString()
        {
            return this.day.ToString()+ "/" + this.month.ToString() + "/" + this.year.ToString();
        }

        public static int GetDifference(int year_1 ,int year_2)
        {
            return year_1 - year_2;
        }

        public virtual void AccepDetails()
        {
            Console.WriteLine(" enter day");
            this.day = Convert.ToInt32(Console.ReadLine());


            Console.WriteLine("enter month");
            this.month = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("enter year");
            this.year= Convert.ToInt32(Console.ReadLine());
        }

    }


  
   

   
}
 