﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class Employee : Person
    {
        // (Auto Generated)

        
        private int _id;

        public int id
        {
            get { return _id; }
            set { _id = value; }
        }



        private double _salary;

        public double salary
        {
            get { return _salary; }
            set { _salary = value; }
        }

        // hr (40% of salary);

        private double _hr;

        public double hr
        {
            get { return _hr; }
            set { _hr = (this.salary*40)/100; }
        }

        //da (10% of salary);

        private double _da;

        public double da
        {
            get { return _da; }
            set { _da = (this.salary * 10 )/ 100; }
        }


        private string _designation;

        public string designation
        {
            get { return _designation; }
            set { _designation = value; }
        }

       

      


        public Employee()
        {
            this.id = 0;
            this.salary = 0;
            this.designation = " ";
            

        }

        private EmployeeTypes _EmpTypes;


        private string  _Passcode;

        public string  Passcode
        {
            get { return _Passcode; }
            set { _Passcode = value; }
        }
        private int _Detno;

        public int Deptno
        {
            get { return _Detno; }
            set { _Detno = value; }
        }
        private Date _Hiredate;

        public Date Hiredate
        {
            get { return _Hiredate; }
            set { _Hiredate = value; }
        }



        public Employee(string Name, string Address, bool Gender, int day, int month, int year, double salary, string designation,int deptNo,string passcode,Date hiredate ) : base(Name, Address, Gender, day, month, year)
        {

            int ID = 0;
            this.id = Interlocked.Increment(ref ID);
            this.salary = salary;
            this.designation = designation;
            this.Deptno = deptNo;
            this.Passcode = passcode;
            this.Hiredate = hiredate;
           
        }

        private double _NetSalary;

       /* public double NetSalary
        {
            get { return _NetSalary; }
            set { _NetSalary = this.salary+this.hr+this.da; 
        }*/

        public virtual double NetSalary()
        {
            return this.salary + this.hr + this.da;
        }
        public override string ToString()
        {
            return base.ToString()+"your id" +this.id.ToString() +" your salary: " +this.salary.ToString()+" desgination: "+this.designation ;
        }

        public override void AccepDetails()
        {
            base.AccepDetails();
            Console.WriteLine("enter ur salary");
            this.salary = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("enter ur designation");
            this.designation = Convert.ToString(Console.ReadLine());
          

            Console.WriteLine("Please Enter Trainee, Temporary or Permanent");
         
            string s = Console.ReadLine();

            if (s == "Trainee")
            {
                this._EmpTypes = EmployeeTypes.Trainee;
            }
            else if (s == "Temporary")
            {
                this._EmpTypes = EmployeeTypes.Temporary;
            }
            else if (s == "Permanent")
            {
                this._EmpTypes = EmployeeTypes.Permanent;
            }
            else
            {
                this._EmpTypes = 0;
            }
            Console.WriteLine("enter dept no.");
            this.Deptno = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("enter passcode");
            this.Passcode = Console.ReadLine();
          /*  Console.WriteLine("enter date");
            this.Hiredate = Convert.ToDateTime .ToString( Console.ReadLine());*/

            
        }
    }
}
