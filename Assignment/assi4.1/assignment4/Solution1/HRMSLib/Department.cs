﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class Department
    {
        private int _deptNo;

        public int deptNo
        {
            get { return _deptNo; }
            set { _deptNo = value; }
        }
        private string _deptName;

        public string deptName
        {
            get { return _deptName; }
            set { _deptName = value; }
        }

        private string _location;

        public string location
        {
            get { return _location; }
            set { _location = value; }
        }

        public Department()
        {
            this._deptNo = 0;
            this._deptName = "";
            this._location = "";
        }public Department(int deptNo,string deptName,string loction)
        {
            this.deptNo = deptNo;
            this.deptName = deptName;
            this.location = location;
        }

        public void Acceptdetails()
        {
            Console.WriteLine("enter department no.");
            this.deptNo = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("enter department name ");
            this.deptName = Console.ReadLine();  
            Console.WriteLine("enter location  ");
            this.location = Console.ReadLine();
        }

        public override string ToString()
        {
            return this.deptNo.ToString() +" department name "+ this.deptName +" loction is "+ this.location;
        }



    }
}
