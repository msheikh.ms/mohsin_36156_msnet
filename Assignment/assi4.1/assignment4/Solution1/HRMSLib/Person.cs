﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class Person : Date
    {
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private bool _Gender;

        public bool Gender
        {
            get { return _Gender; }
            set
            {
                _Gender = value;
            }
        }


        private string _Address;

        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }

        private string _Email;

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }


        private string _Password;

        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }

        public Person()
        {
            this._Address = "";
            this._Gender = true;
            this._Name = "";
            this._Email = "";
            this._Password = "";

        }
        public Person(string Name, string Address, bool Gender, int day, int month, int year ):base(day,month,year)
        {
            this.Name = Name;
            this.Address = Address;
            this.Gender = Gender;
         
        

        }

        public string gender()
        {
            return this.Gender == true ? "female" : "male";
        }
        public override string ToString()
        {

            return "Name: " + this.Name + " Gender: " + gender() + " Address: " + this.Address +" email: "+this.Email +" password: "+this.Password + " BirthDay: " + base.ToString();
        }

        public override void AccepDetails()
        {

            Console.WriteLine("enter ur name: ");
            this.Name = Console.ReadLine();
            Console.WriteLine("select option: \n0.Male \n1.Female");
           
            if (Console.ReadLine() == "female") { this.Gender = true; }
            else { this.Gender = false; }

            Console.WriteLine("enter email");
            this.Email = Console.ReadLine();
            Console.WriteLine("enter password");
            this.Password = Console.ReadLine();
            Console.WriteLine("enter ur Address: ");
            this.Address = Console.ReadLine();

            base.AccepDetails();

        }

    }


}
