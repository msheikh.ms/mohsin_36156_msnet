﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
     public class WageEmp: Employee
    {
        
        private int _hours;

        public int hours
        {
            get { return _hours;}
            set { _hours = value; }
        }
        private int _rate;

        public int rate
        {
            get { return _rate; }
            set { _rate = value; }
        }

        public WageEmp()
        {
            this._rate = 0;
            this._hours = 0;
        }
        public WageEmp(string Name, string Address, bool Gender, int day, int month, int year, double salary, string designation,int hours,int rate, int deptNo, string passcode, Date hiredate) :base(Name, Address, Gender, day, month, year,salary,designation,deptNo,passcode,hiredate)
        {
            this.hours = hours;
            this.rate = rate;
        }
        public override double NetSalary()
        {
            return this.salary*this.hours*this.rate ;
        }
        public override string ToString()
        {
            return base.ToString() + " Netsalary ";
        }
        public override void AccepDetails()
        {

            base.AccepDetails();
            Console.WriteLine("enter hours");
            this.hours = Convert.ToInt32(Console.ReadLine());     
            Console.WriteLine("enter rate");
            this.rate = Convert.ToInt32(Console.ReadLine());

        }
    }
}
