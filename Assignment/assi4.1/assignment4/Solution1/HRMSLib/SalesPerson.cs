﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
   public class SalesPerson:Employee
    {
        private double _commission;


        public double commission
        {
            get { return _commission; }
            set { _commission = value; }
        }

        public SalesPerson()
        {
            this._commission = 0;
        }
        public SalesPerson(string Name, string Address, bool Gender, int day, int month, int year, int id, double salary, string designation,double commission, int deptNo, string passcode, Date hiredate) :base(Name, Address, Gender, day, month, year,salary,designation, deptNo, passcode, hiredate)
        {
            this.commission = commission;
        }

      

       
        public override double NetSalary()
        {
            return this.salary + this.hr + this.da + this.commission; ;
        }
        public override string ToString()
        {
            return base.ToString()+" NetSalary "+ NetSalary().ToString();
        }

        public override void AccepDetails()
        {

            base.AccepDetails();
            Console.WriteLine("enter commission");
            this.commission = Convert.ToDouble(Console.ReadLine());
            
        }
    }
}
