			Abstract                                                              Interface
1.It contains both declaration and definition part.					It contains only a declaration part

2.Multiple inheritance is not achieved by abstract class.				Multiple inheritance is achieved by interface.

3.It contain constructor.								It does not contain constructor.

4.It can contain static members.							It does not contain static members.


5.It can contain different types of access modifiers					It only contains public access modifier because everything in the interface is public.
 like public, private, protected etc.

6.The performance of an abstract class is fast.						The performance of interface is slow because it requires time
											 to search actual method in the corresponding class.

7.It is used to implement the core identity of class.					It is used to implement peripheral abilities of class.

8.A class can only use one abstract class.						A class can use multiple interface.

9.If many implementations are of the same kind and use common behavior, 		If many implementations only share methods, then it is superior to use Interface.
then it is superior to use abstract class.

10.Abstract class can contain methods, fields, constants, etc.				Interface can only contain methods .

11.It can be fully, partially or not implemented.					It should be fully implemented.