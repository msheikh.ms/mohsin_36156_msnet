﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace arraylist
{
    class Program
    {
        static void Main(string[] args)
        {
            emp e1 = new emp();
            e1.Name = "raj";
            e1.No = 85;
            emp e2 = new emp();
            e2.Name = "ramesh";
            e2.No = 96;
            emp e3 = new emp();
            e3.Name = "suresh";
            e3.No = 32;

            ArrayList arr = new ArrayList();
            arr.Add(e1);
            arr.Add(e2);
            arr.Add(e3);
            arr.Add(100);
            arr.Add("lkjhg");

            for (int i = 0; i < arr.Count; i++)
            {
                if (arr[i] is int)
                {
                    int k = Convert.ToInt32(arr[i]);
                    Console.WriteLine(k);
                }
                else if (arr[i] is string)
                {
                    Console.WriteLine(arr[i]);
                }
                else if (arr[i] is emp)
                {
                    emp e = (emp)arr[i];
                    Console.WriteLine(e.getdetails());
                }
                else
                {
                    Console.WriteLine("unknown datatype");
                }

            }
            Console.ReadLine();

        }
    }
    public class emp
    {
        private string name;
        private int no;

        public int No
        {
            get { return no; }
            set { no = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string getdetails()
        {

            return this.No.ToString() + this.Name;

        }
    }
}
