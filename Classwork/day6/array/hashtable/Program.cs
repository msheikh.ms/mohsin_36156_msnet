﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hashtable
{
    class Program
    {
        static void Main(string[] args)
        {
            emp e1 = new emp();
            e1.Name = "raj";
            e1.No = 85;
            emp e2 = new emp();
            e2.Name = "ramesh";
            e2.No = 96;
            emp e3 = new emp();
            e3.Name = "suresh";
            e3.No = 32;



            Hashtable arr = new Hashtable();
            arr.Add(1, e1);
            arr.Add("b", e2);
            arr.Add(10.2, e3);

            foreach (object key in arr.Keys)
            {
                Console.WriteLine("key:"+ key);
                object obj = arr[key];
                if (obj is int)
                {
                    int k = Convert.ToInt32(obj);
                    Console.WriteLine(k);
                }
                else if (obj is string)
                {
                    Console.WriteLine(obj);
                }
                else if (obj is emp)
                {
                    emp e = (emp)obj;
                    Console.WriteLine(e.getdetails());
                }
                else
                {
                    Console.WriteLine("unknown datatype");
                }

            }
            Console.ReadLine();



        }
    }
    public class emp
    {
        private string name;
        private int no;

        public int No
        {
            get { return no; }
            set { no = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string getdetails()
        {
           string detail = "Name:"+this.Name   +       "    NO:"+this.No;

            /*            return this.No.ToString() + this.Name;
            */
            return detail;
        }
    }
}
