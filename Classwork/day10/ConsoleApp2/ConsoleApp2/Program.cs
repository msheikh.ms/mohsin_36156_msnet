﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        //for anonymous method//
       public delegate string myDeligate(string name);
       /* var v3 = 100;*/
       // var cannot declare globaly it can only be decclare localy

        static void Main(string[] args)
        {
            #region partial classes
            /* maths m = new maths();
             Console.WriteLine(m.Add(10, 5));
             Console.WriteLine(m.sub(10, 5));
             Console.ReadLine();*/
            #endregion


            #region nullable

            // int salary = null;
            //here the error is "cannot convert int to null , non nullable vlaue"

            /*            Nullable<int> salary = null;
            */
            //now it is allowing it to do so because we made it nullable 

            //same happend when 

            /*      int? salary1 = null;*/

            // by this we can make it nullable 

            // syntax is int? ( we just have to apply ? after int or after dataatype)








            #endregion


            #region anonymous method
            //traditional method 


            /*  string msg = hello("ramesh");
              Console.WriteLine(msg);
              Console.ReadLine();*/


            //traditinal method
            /* public static string hello(string name)
             {
                 return "hello " + name + "  in programing";
             }*/

            //another method of calling function

            /*         myDeligate pointer = new myDeligate(hello);
                     string msg1 = pointer("naresh");
                     Console.WriteLine(msg1);
                     Console.ReadLine();*/

            // anonymous method is a method without name


            /*

                         myDeligate pointer2 = delegate (string name)
                                                 {
                                                     return "hello " + name;
                                                 };*/

            //the method is written within it only//
            // string msg = pointer2("suresh");
            // Console.WriteLine(msg);








            #endregion


            #region lambada expression
            //lambada expression uses arrow function 

            /* myDeligate pointer = (name) => {
                                             return "hello " + name + " from lambada expression";
                                             };

             string msg = pointer("rajesh");
             Console.WriteLine(msg);
             Console.ReadLine();

 */

            #endregion


            #region itterator
       /*     Week we = new Week();
            foreach (string day in we)
            {
                Console.WriteLine(day);
            }
            Console.ReadLine();
*/

            #endregion

            #region implecite type var

            var i = 100;
            i = 200;
            //  i = "hsbs";
            // error   variabl connot convert string to int


            // so var depend on righ hand side to get its datatype 
            //depending on right hand side it deside  its datatype


            #endregion


            #region Auto property
            /* //compiler is not making class for us here
             emp e = new emp() { No = 100, Name = "rahul", address = "pune" };

             Console.WriteLine(e.Name);
             Console.WriteLine( e.address);
             Console.WriteLine(e.No);


             //copiler is creating class for us

             var employy = new { no = 100, name = "pooja", addres = "nagpur" };
             Console.WriteLine(employy.name);
             Console.WriteLine(employy.addres);
             Console.ReadLine();
            // employy.no = 200;         //eeror

             //cannot assigend , readonly
             // var is anonymus type object
 */




            #endregion


            #region anonymous type

            var prop1 = new { no = 100, name = "raj", sal = 2000 };
            var prop2 = new { no = 100, name = "raju", sal = 3000 };

            //there is two prop (prop1,prop2) but only one class is created
            //because sequence is same of both and value is generic 
            //if we change the sequence than different class will be created



            #endregion

        }




    }


public class emp
    {
        public int No { get; set; }
        public string Name { get; set; }
        public string address { get; set; }
    }














    public partial class maths
    {
        public int Add( int x , int y)
        {
            return x+y;

        }
    }
    public partial class maths
    {
        public int sub(int x,int y)
        {
            return x - y;
        }
    }

    public class Week:IEnumerable
    {
        public string[] days = new string[] { "mon", "tue", "wed", "thu", "fri", "sat", "sun" };

        public IEnumerator GetEnumerator()
        {
            for (int i = 0; i < days.Length; i++)
            {
              yield  return days[i];
            }
            


        }



    }
}
