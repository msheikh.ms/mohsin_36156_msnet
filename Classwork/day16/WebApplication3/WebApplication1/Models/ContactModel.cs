﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class ContactModel
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string email { get; set; }
        public string Query { get; set; }

        public override string ToString()
        {
            return string.Format("we have recived the containt with name={0}, phone= {1}, email={2},query={3}",
                                this.Name, this.Phone, this.email, this.Query);
        }
    }
}