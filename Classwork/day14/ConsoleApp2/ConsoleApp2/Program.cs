﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            sunbeamDBEntities dBEntities = new sunbeamDBEntities();

            #region select 
            /* var allEmp = dBEntities.Emp11.ToList();
             foreach (var item in allEmp)
             {
                 Console.WriteLine(item.Name+" is from "+item.Address);
             }

             Console.ReadLine();*/

            #endregion

            #region insert
            /* dBEntities.Emp11.Add(new Emp11() { No = 7, Name = "payal", Address = "vidhisha" });
             dBEntities.SaveChanges();
 */


            #endregion

            #region update
            /*
                        var empToUpdate = (from Emp11 in dBEntities.Emp11.ToList()
                                           where Emp11.No == 2
                                           select Emp11).First();
                        empToUpdate.Name = "parleG";
                        empToUpdate.Address = "biscut";
                        dBEntities.SaveChanges();

                        Console.ReadLine();*/
            #endregion

            #region delete
            /* var empToDelete = (from Emp11 in dBEntities.Emp11.ToList()
                                where Emp11.No == 2
                                select Emp11).First();
             dBEntities.Emp11.Remove(empToDelete);
             dBEntities.SaveChanges();*/
            #endregion

            #region calling stored procedure

            /*            dBEntities.spInsert(8, "simbha", "goa");
            */
            #endregion

           var em=  dBEntities.Emps.ToList();

            foreach (var item in em)
            {
                Console.WriteLine(item.Name+"-"+item.Address);
            }
            Console.ReadLine();
        }
    }
}
