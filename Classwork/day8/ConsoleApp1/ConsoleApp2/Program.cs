﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            #region array serialization
           /* Emp e = new Emp();
            Console.WriteLine("enter name of emp");
            e.Name = Console.ReadLine();
            Console.WriteLine("enter no of employee");
            e.NO = Convert.ToInt32(Console.ReadLine());


            books b = new books();
            Console.WriteLine("enter title of book");
            b.title = Console.ReadLine();
            Console.WriteLine("enter author of book");
            b.author = Console.ReadLine();

            //binary serialization of arraylist

            ArrayList arr = new ArrayList();
            arr.Add(e);
            arr.Add(b);

            FileStream fs = new FileStream(@"D:\mohsin_36156_msnet\Classwork\day8\arraylist.txt", FileMode.OpenOrCreate, FileAccess.Write);

            BinaryFormatter swrittert = new BinaryFormatter();

            swrittert.Serialize(fs, arr);

            swrittert = null;
            fs.Flush();
            fs.Close();

            Console.ReadLine();*/
            #endregion



            #region array deserializatio

            FileStream fs = new FileStream(@"D:\mohsin_36156_msnet\Classwork\day8\arraylist.txt", FileMode.Open, FileAccess.Read);
            BinaryFormatter sread = new BinaryFormatter();

            object obj = sread.Deserialize(fs);

           if(obj is ArrayList)
            {
                ArrayList arr = (ArrayList)obj;
                foreach (object o in arr)
                {
                    if (o is Emp)
                    {
                        Emp e = (Emp)o;
                        Console.WriteLine(e.getDetails());
                    }
                    else if (o is books)
                    {
                        books b = (books)o;
                        Console.WriteLine(b.getDetails());
                    }
                    else
                    {
                        Console.WriteLine("unknown data");
                    }
                }
            }

            Console.ReadLine();
            #endregion
        }
    }
    [Serializable]
    public class  books
    {
        private string Title;
        private string Author;

        public string author
        {
            get { return Author; }
            set { Author = value; }
        }

        public string title
        {
            get { return Title; }
            set { Title = value; }
        }

        public string getDetails()
        {
            return "tittle of book is  " + title + "  " + "author of book is " + author;
        }

    }





    [Serializable]
    public class Emp
    {
        private int no;
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int NO
        {
            get { return no; }
            set { no = value; }
        }
        public string getDetails()
        {
            return "name of employee   " + name + "    " + "Employee no is :" + no;
        }
    }

}
