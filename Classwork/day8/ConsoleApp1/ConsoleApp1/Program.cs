﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*  Emp e = new Emp();
              Console.WriteLine("enter name of emp");
              e.Name = Console.ReadLine();
              Console.WriteLine("enter no of employee");
              e.NO = Convert.ToInt32(Console.ReadLine());*/


            // binary serialization
            /*FileStream fs = new FileStream(@"D:\mohsin_36156_msnet\Classwork\day8\file1.txt", FileMode.OpenOrCreate,FileAccess.Write);
            BinaryFormatter swritter = new BinaryFormatter();
            swritter.Serialize(fs, e);
            swritter = null;
            fs.Flush();
            fs.Close();*/


            //binary Deserializaion

            FileStream fs = new FileStream(@"D:\mohsin_36156_msnet\Classwork\day8\file1.txt", FileMode.Open, FileAccess.Read);
            BinaryFormatter sreader = new BinaryFormatter();

            object obj = sreader.Deserialize(fs);

            if (obj is Emp)
            {
                Emp e = (Emp)obj;
                Console.WriteLine(e.getDetails());
            }
            else
            {
                Console.WriteLine("unknow data");
            }


            Console.ReadLine();
        }
    }
    [Serializable]
    public class Emp
    {
        private int no;
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int NO
        {
            get { return no; }
            set { no = value; }
        }
        public string getDetails()
        {
            return "name of employee   " + name + "    " + "Employee no is :" + no;
        }
    }
}
