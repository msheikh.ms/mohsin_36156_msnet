﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Diagnostics;
namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch watch = new Stopwatch();
            #region func delegate
            /*Func<int, bool> pointer = delegate (int i)
            {
                return i > 10;
            };
            bool result = pointer(100);
            Console.ReadLine()*/
            #endregion

            #region func lamabada
            /*
                        watch.Start();
                        #region func lambada

                        Func<int ,bool> pointer = (i) =>
                        {
                            return (i > 10);
                        };
                        bool result = pointer(400);
                        watch.Stop();

                        Console.WriteLine(result);
                        Console.WriteLine("time taken = "+watch.ElapsedTicks.ToString());
                        Console.ReadLine();


            */
            #endregion


           /* Func<int, bool> pointer = (i) =>
            {
                return (i > 10);
            };*/
            Expression<Func<int, bool>> tree = (i) => (i > 100) ;
            Func<int,bool> pointer = tree.Compile();
            bool result = pointer(500);
            Console.WriteLine(result);
            Console.ReadLine();
        }
    }
}
