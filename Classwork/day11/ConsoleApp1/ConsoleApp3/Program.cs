﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            //linq
            emp em = new emp();
            Console.WriteLine("enter what u want to search");
            string cityfilter =Console.ReadLine();
            List<emp> emps = new List<emp>()
            {
                new emp {NO = 11,Name="ank",Addess="pune"},
                new emp {NO = 12,Name="abhi",Addess="nagpur"},
                new emp {NO = 13,Name="shubham",Addess="ambazari"},
                new emp {NO = 14,Name="ashu",Addess="katol"},
                new emp {NO = 15,Name="naaresh",Addess="pune"}

            };
            #region normal method
            /* var result = new List<emp>();
             foreach (emp em in emps)
             {
                 if (em.Addess.Contains(cityfilter))
                 {
                     result.Add(em);
                 }
             }*/
            #endregion

            #region m1
            /*    var result = (from emp in emps
                              where emp.Addess.StartsWith(cityfilter)
                              select emp).ToList();*/
            #endregion
            var result = (from emp in emps
                          where emp.Addess.StartsWith(cityfilter)
                          select new
                          {
                              EName = "Mr / Mrs " + emp.Name,
                              ECity = emp.Addess
                          }).ToList();
                          




            Console.WriteLine("result of your que");

            foreach (var E in result)
            {
                Console.WriteLine(E.EName + " is from " + E.ECity);
            }

            /* foreach (var e in result)
             {
                 Console.WriteLine(e.EName + " is from " + e.ECity);
             }*/
            Console.ReadLine();


        }
    }
    public class emp
    {
        public int NO { get; set; }
        public string  Name { get; set; }
        public string Addess { get; set; }
    }
}
