﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using System.Configuration;
using WebApplication1.Filter;

namespace WebApplication1.Controllers
{
    public class homeeController : BaseController
    {
/*        sunbeamDBEntities dBEntities = new sunbeamDBEntities();
*/
        // GET: homee
        public ActionResult Index()
        {
            ViewBag.MyTitle = "welcome home";
        
            var allEmps = dBEntities.Emp11.ToList();
            return View(allEmps);
            
        }



        public ActionResult Delete(int id)
        {

            Emp11 empToBeDeleted = (from emp in dBEntities.Emp11.ToList()
                                    where emp.No == id
                                    select emp).First();
            dBEntities.Emp11.Remove(empToBeDeleted);
            dBEntities.SaveChanges();
            return Redirect("/homee/Index");

        }

        public ActionResult Edit(int id)
        {

            ViewBag.Message = "Please Update Record Here!";

            Emp11 empToBeEdited = (from emp in dBEntities.Emp11.ToList()
                                   where emp.No == id
                                   select emp).First();

            return View(empToBeEdited);
        }

        [HttpPost]
        public ActionResult Edit(Emp11 empUpdated)
        {
            var v = ViewBag.Message;
            Emp11 empToBeEdited = (from emp in dBEntities.Emp11.ToList()
                                   where emp.No == empUpdated.No
                                   select emp).First();

            empToBeEdited.Name = empUpdated.Name;
            empToBeEdited.Address = empUpdated.Address;

            dBEntities.SaveChanges();

            return Redirect("/homee/Index");
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Emp11 empToBeAdded)
        {
            dBEntities.Emp11.Add(empToBeAdded);
            dBEntities.SaveChanges();

            return Redirect("/homee/Index");
        }

        public ActionResult About()
        {
            ViewBag.MyTitle = "About us";

            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.MyTitle = "contact us";
            ViewBag.action = "/homee/Contact";
            ViewBag.method = "POST";
            ViewBag.message = "error here";

            return View();
        }
        [HttpPost]
        public ActionResult Contact(ContactModel contactDetails)
        {
            string emailname = ConfigurationManager.AppSettings["email"];
            string password = ConfigurationManager.AppSettings["password"];

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(emailname);
            mail.To.Add(contactDetails.email);
            mail.Subject = "how to mail";
            mail.Body = contactDetails.ToString();
            mail.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);

            smtp.Credentials = new NetworkCredential(emailname, password);
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.EnableSsl = true;

            smtp.Send(mail);

            ViewBag.massage = "query submited";
            return View ();
        }
    }
}