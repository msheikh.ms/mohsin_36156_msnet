﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;


namespace WebApplication1.Controllers
{
    public class SampleController : BaseController
    {
        // GET: Sample

        public ActionResult Index()
        {
            ViewBag.MyTitle = "welcome home";

            var allEmps = dBEntities.Emp11.ToList();
            return View(allEmps);

        }



        public ActionResult Delete(int id)
        {

            Emp11 empToBeDeleted = (from emp in dBEntities.Emp11.ToList()
                                    where emp.No == id
                                    select emp).First();
            dBEntities.Emp11.Remove(empToBeDeleted);
            dBEntities.SaveChanges();
            return Redirect("/Sample/Index");

        }

        public ActionResult Edit(int id)
        {

            ViewBag.Message = "Please Update Record Here!";

            Emp11 empToBeEdited = (from emp in dBEntities.Emp11.ToList()
                                   where emp.No == id
                                   select emp).First();

            return View(empToBeEdited);
        }

        [HttpPost]
        public ActionResult Edit(Emp11 empUpdated)
        {
            var v = ViewBag.Message;
            Emp11 empToBeEdited = (from emp in dBEntities.Emp11.ToList()
                                   where emp.No == empUpdated.No
                                   select emp).First();

            empToBeEdited.Name = empUpdated.Name;
            empToBeEdited.Address = empUpdated.Address;

            dBEntities.SaveChanges();
             
            return Redirect("/Sample/Index");
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Emp11 empToBeAdded)
        {
            if (ModelState.IsValid)
            { 
            dBEntities.Emp11.Add(empToBeAdded);
            dBEntities.SaveChanges();

            return Redirect("/Sample/Index");
            }
            else
            {
                return View(empToBeAdded);
            }
        }

        public ActionResult About()
        {
            ViewBag.MyTitle = "About us";

            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.MyTitle = "contact us";
            ViewBag.action = "/homee/Contact";
            ViewBag.method = "POST";
            ViewBag.message = "error here";

            return View();
        }
       
    }
}