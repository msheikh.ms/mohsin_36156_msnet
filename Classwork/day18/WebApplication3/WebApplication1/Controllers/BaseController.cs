﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using WebApplication1.Filter;

namespace WebApplication1.Controllers
{
    [Authorize]
    [customFilter]
    [HandleError(ExceptionType =typeof(Exception),View ="Error")]
    public class BaseController : Controller
    {
        protected sunbeamDBEntities dBEntities { get; set; }
        // GET: Base
        public BaseController()
        {
            this.dBEntities = new sunbeamDBEntities();
        }
    }
}