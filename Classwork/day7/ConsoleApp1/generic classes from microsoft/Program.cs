﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace generic_classes_from_microsoft
{
    class Program
    {
        static void Main(string[] args)
        {

            Emp e1 = new Emp();
            e1.No = 1;
            e1.Name = "Rajiv";

            Emp e2 = new Emp();
            e2.No = 2;
            e2.Name = "Rahul";

            Emp e3 = new Emp();
            e3.No = 3;
            e3.Name = "Mahesh";

            //list 
            List<Emp> arr = new List<Emp>();
            arr.Add(e1);
            arr.Add(e2);

            foreach (Emp emp  in arr)
            {
                Console.WriteLine("list");
                Console.WriteLine(emp.getDetails());
            }


            //stack

            Stack<Emp> arr1 = new Stack<Emp>();
            arr1.Push(e1);
            arr1.Push(e3);

            foreach (Emp emp1 in arr1)
            {
                Console.WriteLine("stack");
                Console.WriteLine(emp1.getDetails());
            }



            //queue

            Queue<Emp> arr2 = new Queue<Emp>();

            arr2.Enqueue(e1);
            arr2.Enqueue(e2);
            arr2.Enqueue(e3);

            foreach (Emp emp2 in arr2)
            {
                Console.WriteLine("queue");
                Console.WriteLine(emp2.getDetails());
            }

            Console.ReadLine();


        }
        public class Emp
        {
            private int _No;
            private string _Name;

            public string Name
            {
                get { return _Name; }
                set { _Name = value; }
            }

            public int No
            {
                get { return _No; }
                set { _No = value; }
            }

            public string getDetails()
            {
                return this.No.ToString() + this.Name;
            }

        }
    }
}






