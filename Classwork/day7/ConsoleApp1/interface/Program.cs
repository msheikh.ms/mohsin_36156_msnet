﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace @interface
{
    class Program
    {
        static void Main(string[] args)
        {
            A obj = new C();
            Console.WriteLine(obj.add(10,50));
            B obj1 = new C();
            Console.WriteLine(obj1.mul(10,56));
            Console.ReadLine();

        }
    }

    public interface A
    {
        int add(int x, int y);
        int sub(int x, int y);
    }
     
    public interface B
    {
        int add(int x, int y);
        int mul(int x, int y);
    }

    public class C : A, B
    {
        int A.add(int x, int y)
        {
            return x + y;
        }

        int B.add(int x, int y)
        {
            return x + y + 100;
        }

        int B.mul(int x, int y)
        {
            return x * y;
        }

        int A.sub(int x, int y)
        {
            return x - y;
        }
    }






}
