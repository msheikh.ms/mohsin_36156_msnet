﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace fileIO
{
    class Program
    {
        static void Main(string[] args)
        {

            /*//writting into file //


            FileStream papper = new FileStream(@"D:\mohsin_36156_msnet\Classwork\New folder\helloworld.txt", FileMode.OpenOrCreate, FileAccess.Write);
            //remember to put file name and file extension in path 
            StreamWriter pen = new StreamWriter(papper);
            pen.WriteLine("hello World");
            papper.Flush();
            pen.Close();
            papper.Close();
*/

            // reading from file


            FileStream papper = new FileStream(@"D:\mohsin_36156_msnet\Classwork\New folder\helloworld.txt", FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(papper);
            string data = reader.ReadToEnd();
            Console.WriteLine(data);
            reader.Close();
            papper.Close();
            Console.ReadLine();
            
        }
    }
}
