﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Emp e1 = new Emp();
            e1.No = 1;
            e1.Name = "Rajiv";

            Emp e2 = new Emp();
            e2.No = 2;
            e2.Name = "Rahul";

            Emp e3 = new Emp();
            e3.No = 3;
            e3.Name = "Mahesh";

            Maths obj = new Maths();
            int p = 6;
            int q = 8;

            Console.WriteLine("befor swap p= " + p.ToString()+"q= "+q.ToString());

            obj.swap<int>(ref p,ref q);


            Console.WriteLine("after swap p= " + p.ToString() + "q= " + q.ToString());

            obj.me();
            Console.ReadLine();
        }
    }
    public class Emp
    {
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string getDetails()
        {
            return this.No.ToString() + this.Name;
        }

    }
    public class Maths
    {
        public void swap<T>(ref T x, ref T y)
        {
            T z;
            z = x;
            x = y;
            y = z;
        }
        public void me()
        {
            Console.WriteLine(  "hello");
        }
    }
}
