﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyOwnAttribute;

namespace POCO
{
    [Table(TableName ="Employee")]
    public class Emp
    {
        private int _No;
        private string _Name;
        [Column(ColumnName ="EName",ColumnType ="varchar(100)")]
        public string Name
        {
            get { return _Name; } 
            set { _Name = value; }
        }

        [Column(ColumnName = "ENo",ColumnType ="int")]
        public int NO
        {
            get { return _No; }
            set { _No = value; }
        }

    }
}
