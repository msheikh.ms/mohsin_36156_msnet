﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using MyOwnAttribute;

namespace dbGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("enter the path of your class libary");
            string path = Console.ReadLine();
            Assembly assembly = Assembly.LoadFrom(path);

            Type[] alltypes = assembly.GetTypes();

            string query = "";

            foreach (Type type in alltypes)
            {
                List<Attribute> allattributes = type.GetCustomAttributes().ToList();
                //table attribute//
                foreach (Attribute attribute in allattributes)
                {
                    if(attribute is Table)
                    {
                        Table tableAttributeObject = (Table)attribute;
                        string tableName = tableAttributeObject.TableName;
                        query = query + "create table" + tableName + " ( ";
                        break;
                    }
                }

                PropertyInfo[] allGetterSetter = type.GetProperties();
                foreach (PropertyInfo propertyInfo in allGetterSetter) 
                {
                    List<Attribute> allAttributesOnCurrentGetterSetter = propertyInfo.GetCustomAttributes().ToList();

                    foreach (Attribute attributeongettersetter in allAttributesOnCurrentGetterSetter)
                    { 
                        if(attributeongettersetter is Column)
                        {
                            Column columnAttributeObject = (Column)attributeongettersetter;
                            string columnName = columnAttributeObject.ColumnName;
                            string columnType = columnAttributeObject.ColumnType;
                            query = query + columnName + " " + columnType + ",";
                            break;
                        }
                    }

                }

                query = query.TrimEnd(new char[] { ',' });
                query = query + ");";

            }
            Console.WriteLine(query);
            Console.ReadLine();

        }
    }
}
