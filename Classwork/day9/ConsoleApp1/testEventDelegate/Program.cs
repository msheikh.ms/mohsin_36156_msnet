﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using database;

namespace testEventDelegate
{
    class Program
    {
        static void Main(string[] args)
        { 

            sqlServer dbobj = new sqlServer();
            
            mydelegate pointer1 = new mydelegate(ondatainsert);
            mydelegate pointer2 = new mydelegate(ondataupdate);
            mydelegate pointer3 = new mydelegate(ondatadelete);


            dbobj.inserted += pointer1;
            dbobj.updateed += pointer2;
            dbobj.deleted += pointer3;


            dbobj.insert("inserting data");
            dbobj.update("dupdate");
            Console.ReadLine();

        }
        public static void ondatainsert()
        {
            Console.WriteLine("info:-data inserted by someone");
        }
        public static void ondataupdate()
        {
            Console.WriteLine("info:-data updated by someone");
        }
        public static void ondatadelete()
        {
            Console.WriteLine("info:-data deleted by someone");
        }
    }
}

namespace database
{
    public delegate void mydelegate();
    public class sqlServer
    {
        public event mydelegate inserted;
        public event mydelegate updateed;
        public event mydelegate deleted;

        public void insert(string data)
        {
            Console.WriteLine(data+"data inserted successfully");
            inserted();
        }
        public void update(string data)
        {
            Console.WriteLine(data+"updated data successfull");
            updateed();

        }
        public void delete(string data)
        {
            Console.WriteLine(data+"deleting successfull");
            deleted();

        }


    }   


}

