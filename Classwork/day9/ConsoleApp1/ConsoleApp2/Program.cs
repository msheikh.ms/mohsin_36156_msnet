﻿using System;  
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("enter the assembely path ");
            
            string pathofasssembly = Console.ReadLine();

            Assembly assembly = Assembly.LoadFrom(pathofasssembly);

            Type[] allTypes = assembly.GetTypes();

            object dynamicObject = null;
            /*
                        Console.WriteLine(allTypes);
                        Console.ReadLine();
                          //output :-*//*System.Type[]*/

            foreach (Type type in allTypes)
            {
                Console.WriteLine("Type:-"+ type.Name);

                /*  Console.ReadLine();
               D:\mohsin_36156_msnet\Assignment\assi4.1\assignment4\Solution1\HRMSLib\bin\Debug\HRMSLib.dll
               Type:-Department*/

                dynamicObject = assembly.CreateInstance(type.FullName);

                MethodInfo[] allMethods = type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

                foreach (MethodInfo method in allMethods)
                {
                    Console.Write(method.ReturnType+" "+method.Name+" ( ");

                    ParameterInfo[] allparams = method.GetParameters();
                    foreach (ParameterInfo para in allparams)
                    {
                        Console.Write(para.ParameterType.ToString() + "  " + para.Name + "  ");

                    }

                    Console.Write(" ) ");
                    Console.WriteLine();

                    object[] allParas = new object[] { 10, 20 };

                    object result = type.InvokeMember(method.Name,
                                        BindingFlags.Public |
                                        BindingFlags.Instance |
                                        BindingFlags.InvokeMethod,
                                        null, dynamicObject, allParas);

                    Console.WriteLine("Result of " + method.Name + " executed is " + result.ToString());
                    Console.ReadLine();

                    /*output 
                     enter the assembely path
                    D:\mohsin_36156_msnet\Classwork\day9\ConsoleApp1\ClassLibrary1\bin\Debug\ClassLibrary1.dll
                    Type:-Class1
                    System.Int32 add ( System.Int32  x  System.Int32  y   )
                    Result of add executed is 30
                     */


                }


            }



        }
    }
}
