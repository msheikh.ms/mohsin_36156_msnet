﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class testController : Controller
    {
        sunbeamDBEntities dBEntities = new sunbeamDBEntities();

        #region show
        /*  public ActionResult show()
          {
              *//*  Emp11 emp = new Emp11()

                {
                    No = 1,
                    Name = "panasonic",
                    Address = "India"
                };
                return View("xyz", emp);*/

        /* List<Emp11> allEmps = new List<Emp11>()
           {
           new Emp11{ No = 11, Name = "mahesh1", Address = "pune1" },
           new Emp11{ No = 12, Name = "mahesh2", Address = "pune2" },
           new Emp11{ No = 13, Name = "mahesh3", Address = "pune3" },
           new Emp11{ No = 14, Name = "mahesh4", Address = "pune4" }
          };
         return View(allEmps);*//*
    }*/

        #endregion

        public ActionResult edit(int id)
        {
            Emp11 emptoedit = (from emp in dBEntities.Emp11.ToList()
                               where emp.No == id
                               select emp).First();


            return View (emptoedit);

        }

 /*       public ActionResult AfterEdit(FormCollection entireForm)
        { int noofemptoedit = Convert.ToInt32(entireForm["No"]);
            Emp11 emptobeedited = (from emp in dBEntities.Emp11.ToList()
                                   where emp.No == noofemptoedit
                                   select emp).First();

            emptobeedited.Name = entireForm["Name"];
            emptobeedited.Address = entireForm["Address"];

            dBEntities.SaveChanges();

            return Redirect("/test/show");

        }*/

        public ActionResult delete(int id)
        {

            Emp11 emptobedeleted = (from emp in dBEntities.Emp11.ToList()
                                    where emp.No == id
                                    select emp).First();

            dBEntities.Emp11.Remove(emptobedeleted);
            dBEntities.SaveChanges();

            return Redirect("/test/show");
        }

        public ActionResult insert()
        {
            return View ();
        }

        [HttpPost]
        public ActionResult insert(Emp11 emptoadd)
        {


            /*  Emp11 emptoinert = new Emp11()
              {
                  No = Convert.ToInt32(enterform["No"]),
                  Name = enterform["Name"].ToString(),
                  Address = enterform["Address"].ToString()
               };*/

           /* dBEntities.Emp11.Add(emptoinert);*/

            dBEntities.Emp11.Add(emptoadd);
            dBEntities.SaveChanges();
            return Redirect ("/test/show");
        }


        public ActionResult show()
        {
            ViewData["mymessage"] = "welcome to this application";


            ViewBag.msg = "welcome";
            var emp = dBEntities.Emp11.ToList();

            return View(emp);
        }




        [HttpPost]
        public ActionResult edit(Emp11 empupdated)
        {
            Emp11 emptobeedited = (from emp in dBEntities.Emp11.ToList()
                                   where emp.No == empupdated.No
                                   select emp).First();

            emptobeedited.Name = empupdated.Name;
            emptobeedited.Address = empupdated.Address;

            dBEntities.SaveChanges();

            return Redirect("/test/show");

        }
    }
}