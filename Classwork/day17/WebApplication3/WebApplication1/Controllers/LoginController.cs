﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class LoginController : Controller
    {
        sunbeamDBEntities dBEntities = new sunbeamDBEntities();

        // GET: Login
        public ActionResult SignIn()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult SignIn(HRLoginInfo loginObject, string ReturnUrl)
        {
            var Matchcount = (from hr in dBEntities.HRLoginInfoes.ToList()
                              where hr.UserName == loginObject.UserName && hr.password == loginObject.password
                              select hr).ToList().Count();
            if (Matchcount == 1)
            {
                FormsAuthentication.SetAuthCookie(loginObject.UserName, false);

                if (ReturnUrl!= null)
                {
                    return Redirect(ReturnUrl);

                }
                else
                {
                    return Redirect("/homee/Index");
                }
               
            }
            else
            { 
                ViewBag.ErrorMessage = " username or password is not correct";
                return Redirect("/homee/Index");

            }


           
        }
        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return Redirect("/homee/SignIn");
        }
    }
}