﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Helpers;

namespace WebApplication1.Filter
{
    public class customFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Logger.CurrentLogger.Log(string.Format("{0}/{1} is about to ecicute",
            filterContext.ActionDescriptor.ControllerDescriptor.ControllerName, filterContext.ActionDescriptor.ActionName));
        }


        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            Logger.CurrentLogger.Log(string.Format("{0}/{1} is executed",
              filterContext.ActionDescriptor.ControllerDescriptor.ControllerName, filterContext.ActionDescriptor.ActionName));
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            Logger.CurrentLogger.Log("UI proces is about to begin");
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            Logger.CurrentLogger.Log("UI proces is done");

        }
    }
}