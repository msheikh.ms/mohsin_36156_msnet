﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            DataSet ds = new DataSet();

            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=sunbeamDB;Integrated Security=True");

            SqlDataAdapter da = new SqlDataAdapter("select * from Emp11", con);

            da.MissingSchemaAction = MissingSchemaAction.AddWithKey;

            SqlCommandBuilder cmdb = new SqlCommandBuilder(da);

            da.Fill(ds, "Emp");


            //INSERT------------------------------------------------


            #region insert
            /* DataRow newRow = ds.Tables["Emp"].NewRow();

             newRow["No"] = 7;
             newRow["Name"] = "rajesh";
             newRow["Address"] = "mumbai";

             ds.Tables["Emp"].Rows.Add(newRow);

             da.Update(ds, "Emp");

 */
            #endregion

            #region update
            /*
                        Console.WriteLine(  "enter no whose record is to be modified");
                        int tomodify = Convert.ToInt32(Console.ReadLine());

                        DataRow rowToModify = ds.Tables["Emp"].Rows.Find(tomodify);

                        if (rowToModify != null)
                        {
                            Console.WriteLine("current name=" + rowToModify["Name"]);
                            Console.WriteLine("current address=" + rowToModify["Address"]);

                            Console.WriteLine("enter new details");

                            Console.WriteLine("enter new name");
                            rowToModify["Name"] = Console.ReadLine();

                            Console.WriteLine("enter new Address");
                            rowToModify["Address"] = Console.ReadLine();

                        }
                        else
                        {
                            Console.WriteLine("norecord found");
                        }
                        da.Update(ds, "Emp");*/

            #endregion


            #region delete

            Console.WriteLine("enter row you want to delete ");

            int drow = Convert.ToInt32(Console.ReadLine());

            DataRow toDeleteRow = ds.Tables["Emp"].Rows.Find(drow);

            if (toDeleteRow != null)
            {
                toDeleteRow.Delete();
                Console.WriteLine("row deleted successfully");
            }
            else
            {
                Console.WriteLine("no such row found");
            }

            da.Update(ds, "Emp");




            #endregion
        }
    }
}
