﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp1.POCO;
using System.Configuration;
using System.Data.SqlClient;

namespace ConsoleApp1.DAL
{
  
    public class SQLServer:IDatabase
    {

        public List<Emp> Select()
        {
            List<Emp> allEmployees = new List<Emp>();

            string constr = ConfigurationManager.ConnectionStrings["strstring"].ToString();


            SqlConnection con = new SqlConnection(constr);
            SqlCommand cmd = new SqlCommand("select * from Emp11", con);


            con.Open();
            

            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Emp emp = new Emp()
                {
                    No = Convert.ToInt32(reader["No"]),
                    Name = reader["Name"].ToString(),
                    Address = reader["Address"].ToString()
                };
                allEmployees.Add(emp);
            }
            con.Close();
            return allEmployees;
        }

       

        public int Insert(Emp emp)
        {
            string constr = ConfigurationManager.ConnectionStrings["strstring"].ToString();
            SqlConnection con = new SqlConnection(constr);

            string queryformat = "insert into Emp11 values({0},'{1}','{2}')";
            string finalquerey = string.Format(queryformat, emp.No, emp.Name, emp.Address);

            SqlCommand cmd = new SqlCommand(finalquerey, con);

            con.Open();

            int rowAffected = cmd.ExecuteNonQuery();

            con.Close();

            return rowAffected;
        }
        public int Update(Emp emp)
        {
            string constr = ConfigurationManager.ConnectionStrings["strstring"].ToString();
            SqlConnection con = new SqlConnection(constr);

            string queryformat = "update Emp11 set Name='{1}',Address='{2}' where No={0}";
            string finalquerey = string.Format(queryformat, emp.No, emp.Name, emp.Address);

            SqlCommand cmd = new SqlCommand(finalquerey, con);

            con.Open();

            int rowUpdated = cmd.ExecuteNonQuery();

            con.Close();
            return rowUpdated;
        }
        public int Delete(Emp emp)
        {
            string constr = ConfigurationManager.ConnectionStrings["strstring"].ToString();
            SqlConnection con = new SqlConnection(constr);

            string queryformat = "delete from Emp11 where NO={0}";
            string finalquerey = string.Format(queryformat, emp.No);

            SqlCommand cmd = new SqlCommand(finalquerey,con);
            con.Open();

            int rowDeleted = cmd.ExecuteNonQuery();

            con.Close();

            return rowDeleted;


        }
    }
  }

