﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp1.DAL;
using ConsoleApp1.POCO;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            DBFactory dBFactory = new DBFactory();
            IDatabase db = dBFactory.GetDatabase();
            
            Console.WriteLine("Enter your choise");
            Console.WriteLine("1.SELECT   2.INSETRT    3.UPDATE    4.DELETE");
            int opChoice = Convert.ToInt32(Console.ReadLine());

            switch (opChoice)
            {
                case 1:
                    var data = db.Select();
                    foreach (var item in data)
                    {
                        Console.WriteLine(item.Name + " | " + item.Address);
                    }
                    break;
                case 2:
                    Emp emp = new Emp();
                    Console.WriteLine("enter no");
                    emp.No = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("enter Name");
                    emp.Name = Console.ReadLine();
                    Console.WriteLine("enter Address");
                    emp.Address = Console.ReadLine();


                    int rowAffected = db.Insert(emp);

                    Console.WriteLine("no of rows affected ="+ rowAffected.ToString());
                    break;
                case 3:
           
                    Emp emp1 = new Emp();
                    Console.WriteLine("enter no");
                    emp1.No = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("enter Name");
                    emp1.Name = Console.ReadLine();
                    Console.WriteLine("enter Address");
                    emp1.Address = Console.ReadLine();

                    int rowUpdated = db.Update(emp1);

                    Console.WriteLine("no of row updated=" + rowUpdated.ToString());

                    break;
                case 4:
                    Emp emp2 = new Emp();
                    Console.WriteLine("enter the row you want to delete");
                    emp2.No = Convert.ToInt32(Console.ReadLine());

                    int rowDeleted = db.Delete(emp2);

                    Console.WriteLine("row " + rowDeleted + " deleted successfully");
                    
                    break;

                default:
                    Console.WriteLine("plz enter valide option");
                    break;
            }
        }
    }
}
