﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            DataSet ds = new DataSet();


            DataTable table = new DataTable("table1");

            DataColumn col1 = new DataColumn("No", typeof(int));
            DataColumn col2 = new DataColumn("Name", typeof(string));
            DataColumn col3 = new DataColumn("Address", typeof(string));

            table.Columns.Add(col1);
            table.Columns.Add(col2);
            table.Columns.Add(col3);

            table.PrimaryKey = new DataColumn[] { col1 };


            #region datarow entery manualy

            /*
                        DataRow row1 = table.NewRow();

                        row1["No"] = 1;
                        row1["Name"] = "elizabet";
                        row1["Address"] = "england";


                        table.Rows.Add(row1);



                        DataRow row2 = table.NewRow();

                        row2["No"] = 2;
                        row2["Name"] = "smit";
                        row2["Address"] = "england";


                        table.Rows.Add(row2);


                        DataRow row3 = table.NewRow();

                        row3["No"] = 3;
                        row3["Name"] = "blackbeard";
                        row3["Address"] = "england";


                        table.Rows.Add(row3);



                        DataRow row4 = table.NewRow();

                        row4["No"] = 0;
                        row4["Name"] = "blackbeard";
                        row4["Address"] = "england";


                        table.Rows.Add(row4);
            */
            #endregion

            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=sunbeamDB;Integrated Security=True");

            SqlCommand cmd = new SqlCommand("select * from Emp11 ", con);

            con.Open();

            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                DataRow r = table.NewRow();
                r["No"] = Convert.ToInt32(reader["No"]);
                r["Name"] = reader["Name"].ToString();
                r["Address"] = reader["Address"].ToString();

                table.Rows.Add(r);

            }

            DataRow newrowentry = table.NewRow();
            newrowentry["No"] = 6;
            newrowentry["Name"] = "gibs";
            newrowentry["Address"] = "spain";

           table.Rows.Add(newrowentry);


            con.Close();




            ds.Tables.Add(table);









        }
    }
}
