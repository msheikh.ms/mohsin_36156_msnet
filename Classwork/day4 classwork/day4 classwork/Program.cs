﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day4_classwork
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("enter your choise" );
            Console.WriteLine("1.pdf , 2.word , 3.excel");
            int choise = Convert.ToInt32(Console.ReadLine());
            switch (choise)
            {
                case 1:
                    PDF obj1 = new PDF();
                    obj1.Generate();
                    break;
                case 2:
                    WORD obj2 = new WORD();
                    obj2.Generate();

                    break;
                case 3:
                    EXCEL obj3 = new EXCEL();
                    obj3.Generate();

                    break;
                default:
                    break;
            }
            Console.ReadLine();
        }
    }

    public abstract class Report
    {

        protected abstract void Parser();
        protected abstract void Validate();
        protected abstract void save();

        public virtual void Generate()
        {
            Parser();
            Validate();
            save();
        }

    }



    public class PDF : Report
    {
        protected override void Parser()
        {
            Console.WriteLine(" pdf praser is done ");
        }

        protected override void save()
        {
            Console.WriteLine(" pdf save is done ");
        }

        protected override void Validate()
        {
            Console.WriteLine(" pdf validate is done ");
        }
    }




    public class WORD : Report
    {
        protected override void Parser()
        {
            Console.WriteLine(" pdf parser is done ");
        }

        protected override void save()
        {
            Console.WriteLine(" pdf save is done ");
        }

        protected override void Validate()
        {
            Console.WriteLine(" pdf validate is done ");
        }
    }


    public class EXCEL : Report
    {
        protected override void Parser()
        {
            Console.WriteLine(" pdf parsrr is done ");
        }

        protected override void save()
        {
            Console.WriteLine(" pdf save  is done ");
        }

        protected override void Validate()
        {
            Console.WriteLine(" pdf validate is done ");
        }
    }
}
