﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day5_classwork
{
    class Program
    {
        static void Main(string[] args)
        {
            person person = new person();
            person.getsetAge = 50;
            person.getsetName = "rajesh";
            string Details = person.GetDetails();
            Console.WriteLine(Details);
            Console.ReadLine();
                  
        }


        public class person
        {
            #region Privatte member
            private string Name;
            private int Age;
            #endregion

            #region default constructor
            public person()
            {
                this.Name = "";
                this.Age = 0;
            }
            #endregion

            #region paramaterized  constructor
            public person(int age , string name)
            {
                this.Age = age;
                this.Name = name;
            }

            #endregion



            public string getsetName
            {
                get { return this.Name; }
                set { this.Name = value; }
            }

         

            public int getsetAge
            {
                get { return this.Age ; }
                set { this.Age = value; }
            }



            public string GetDetails()
            {
                return "name:"+this.Name + "Age:"+this.Age;
            }
        }
    }
}
