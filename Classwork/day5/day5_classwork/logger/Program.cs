﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logger
{
    class Program
    {
        static void Main(string[] args)
        {
               int x;
               int y;
            Maths m = new Maths();
            logger.currentLogger.log("main is called");
            Console.WriteLine("enter value of x");
            x = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("enter value of y ");
            y = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("sol="+ m.add(x, y)) ;
            Console.ReadLine();
        }
    }


    public class Maths
    {
        public int add(int x, int y)
        {
            logger.currentLogger.log("inside add");
            int result = x + y;
            return result;
        }

    }

    public class logger
    {
        private static logger _logger = new logger();

        public static logger currentLogger
        {
            get { return _logger; }
        }

        public void log(string msg)
        {
            Console.WriteLine("logged :" + msg + "@Time:" + DateTime.Now.ToString());
        }
    }
}
